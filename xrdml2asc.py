#! /usr/bin/env python3

import getopt
import os
import sys


#------------------------------------------------------------------------------
#     CLASSES
#------------------------------------------------------------------------------

class Application:
    """Information about the application."""

    NAME = "XRDML 2 ASC"
    VERSION = "2.0.0"
    DATE = "2022-02-25"


class XrdmlException(Exception):
    """Custom exception for errors that occur while parsing XRDML files."""


class XrdMeasurement:
    """Represents a single XRD measurement. This "record" type is separate
    from Xrdml2Asc since a single XRDML file can contain multiple measurements.
    """

    def __init__(self):
        self.two_theta_start = None
        self.two_theta_end = None
        self.intensities = []
        self.two_theta_step = None


    def calculate_step(self):
        """Calculates 2Theta step from 2Theta scan boundaries and the number
        of measured intensities. The result is saved to `self.two_theta_step`.
        """
        if len(self.intensities) == 0:
            raise XrdmlException("There are no measured intensities")
        if self.two_theta_start is None or self.two_theta_end is None:
            raise XrdmlException("2Theta boundaries are not set")

        diff = self.two_theta_end - self.two_theta_start
        n_points = len(self.intensities)
        self.two_theta_step = diff / (n_points - 1)


class Xrdml2Asc:
    """A self-contained XRDML file reader and processor."""
    
    class Keywords:
        """Keywords used in the XRDML parsing process."""

        MEASUREMENT_START = "<xrdMeasurement"
        MEASUREMENT_END = "</xrdMeasurement>"
        TWO_THETA = "<positions axis=\"2Theta\" unit=\"deg\">"
        TWO_THETA_START_LEFT = "<startPosition>"
        TWO_THETA_START_RIGHT = "</startPosition>"
        TWO_THETA_END_LEFT = "<endPosition>"
        TWO_THETA_END_RIGHT = "</endPosition>"
        INTENSITIES_START = "<intensities unit=\"counts\">"
        INTENSITIES_END = "</intensities>"
    
    
    class Stage:
        """Stages of XRDML parsing process."""

        MEASUREMENT_START = 0
        TWO_THETA_KEYWORD = 1
        TWO_THETA_START = 2
        TWO_THETA_END = 3
        INTENSITIES = 4
        MEASUREMENT_END = 5
  
  
    def __init__(self, file_path: str, output_path: str):
        # Input XRDML file path
        self.__file_path = file_path
        # Exported ASC file path
        self.__output_path = output_path
        
        # Lines read counter
        self.__line_count = None
        # Stage of the XRDML parse process
        self.__stage = self.Stage.MEASUREMENT_START
        # Container for storing parsed measurement data
        self.__measurements = []


    def __parse_theta_value(line: str, left_tag: str, right_tag: str) -> float:
        """Extracts 2Theta value, enclosed by `left_tag` and `right_tag`,
        from `line`. Return the result or throws an exception on failure.

        Example line processed with this method:
        <startPosition>4.00569542</startPosition>
        """
        # Find opening and closing tag positions
        i_start = line.find(left_tag)
        if i_start < 0:
            raise XrdmlException("Could not parse theta value. "
                f"Opening tag '{left_tag}' not found in line '{line}'.")
        i_end = line.rfind(right_tag)
        if i_end < 0:
            raise XrdmlException("Could not parse theta value. "
                f"Closing tag '{right_tag}' not found in line '{line}'.")
        # Extract value
        i_start = i_start + len(left_tag)
        value_str = line[i_start:i_end]
        try:
            value = float(value_str)
            return value
        except ValueError:
            raise XrdmlException("Could not parse theta value. "
                f"Conversion of '{value_str}' to float failed.")


    def __parse_intensities(self, line: str, measurement: XrdMeasurement):
        """Extracts intensity values from given `line`. Intensities are
        expected to be enclosed by appropriate tags. Extracted intensities
        are converted to integer values and appended into provided
        `measurement` object.

        Throws exceptions on failures.
        """
        i_left = line.find(self.Keywords.INTENSITIES_START)
        if i_left < 0:
            raise XrdmlException("Failed to find tag "
                f"'{self.Keywords.INTENSITIES_START}' "
                f"in intensity extraction (line {self.__line_count}).")
        i_right = line.rfind(self.Keywords.INTENSITIES_END)
        if i_right < 0:
            raise XrdmlException("Failed to find tag "
                f"'{self.Keywords.INTENSITIES_END}' "
                f"in intensity extraction (line {self.__line_count}).")

        i_left = i_left + len(self.Keywords.INTENSITIES_START)
        values_str = line[i_left:i_right]
        values_split = values_str.split()

        for value_str in values_split:
            try:
                value = int(value_str)
                measurement.intensities.append(value)
            except ValueError:
                raise XrdmlException("Failed to convert intensity "
                    f"'{value_str}' to integer.")
  

    def __read_xrdml_file(self):
        """Reads the XML file pointed to by the path set by the
        constructor. Throws exceptions in IO or parse process failures.

        Parsed data is saved internally.
        """
        measurement = None
        
        with open(self.__file_path, "r") as xrdml_file:
            self.__line_count = 0

            Debug.log("Entering XRDML parse loop in file "
                f"'{self.__file_path}'")

            for line in xrdml_file:
                line = line.strip()
                self.__line_count += 1
                
                # XRD measurement opening tag
                if self.__stage == self.Stage.MEASUREMENT_START:
                    if self.Keywords.MEASUREMENT_START in line:
                        Debug.log("Found measurement opening tag.")

                        self.__stage = self.Stage.TWO_THETA_KEYWORD
                        measurement = XrdMeasurement()
                # 2Theta keyword
                elif self.__stage == self.Stage.TWO_THETA_KEYWORD:
                    if line == self.Keywords.TWO_THETA:
                        Debug.log("Found 2Theta positions keyword.")
                        self.__stage = self.Stage.TWO_THETA_START
                # 2Theta start
                elif self.__stage == self.Stage.TWO_THETA_START:
                    value = Xrdml2Asc.__parse_theta_value(
                        line,
                        self.Keywords.TWO_THETA_START_LEFT,
                        self.Keywords.TWO_THETA_START_RIGHT
                    )
                    measurement.two_theta_start = value
                    self.__stage = self.Stage.TWO_THETA_END
                    Debug.log(f"Found 2Theta start: {value}")
                # 2Theta end
                elif self.__stage == self.Stage.TWO_THETA_END:
                    value = Xrdml2Asc.__parse_theta_value(
                        line,
                        self.Keywords.TWO_THETA_END_LEFT,
                        self.Keywords.TWO_THETA_END_RIGHT
                    )
                    measurement.two_theta_end = value
                    self.__stage = self.Stage.INTENSITIES
                    Debug.log(f"Found 2Theta end: {value}")
                # Intensities
                elif self.__stage == self.Stage.INTENSITIES:
                    if self.Keywords.INTENSITIES_START in line:
                        self.__parse_intensities(line, measurement)
                        self.__stage = self.Stage.MEASUREMENT_END
                        Debug.log(f"Parsed {len(measurement.intensities)} "
                            "intensities.")
                # XRD measurement closing tag
                elif self.__stage == self.Stage.MEASUREMENT_END:
                    if self.Keywords.MEASUREMENT_END in line:
                        Debug.log("Found measurement closing tag.")
                        measurement.calculate_step()
                        self.__measurements.append(measurement)
                        Debug.log("Calculated step",
                            "{0:.5f}.".format(measurement.two_theta_step),
                            "Appending measurement to collection.")
                        measurement = None
                        self.__stage = self.Stage.MEASUREMENT_START
                # Unknown stage
                else:
                    raise Exception(f"Unknown stage '{self.__stage}' "
                        "in XRDML parse process.")

        Debug.log(f"File read with line count {self.__line_count}")

        # Check if the last measurement was commited
        if measurement != None:
            n_measurement = len(self.__measurements) + 1
            raise XrdmlException(
                f"Measurement {n_measurement} is missing closing tag.")
    
    
    def __write_asc_file(self):
        """Exports the data, acquired from a XRDML file, to an ASC file.
        Output path was defined at object construction.

        If there were multiple measurements, their data is separated by
        two empty lines. Consequently they are recognized as separate
        datasets by Gnuplot.

        The method does not catch potential IO exceptions.
        """
        with open(self.__output_path, "w") as asc_file:
            first = True

            for m in self.__measurements:
                if first:
                    first = False
                else:
                    asc_file.write("\n\n")

                for i, intensity in enumerate(m.intensities):
                    two_theta = m.two_theta_start + i * m.two_theta_step
                    asc_file.write("{0:12.6f} {1:10d}\n"
                                   .format(two_theta, intensity))
    
    
    def convert_file(self):
        """Converts XRDML file to ASC file. File paths were determined at
        object construction.

        Outputs user info data to console.

        The method will not catch exceptions occuring during parse and
        export processes.
        """
        self.__read_xrdml_file()

        print(f"  ⟡ Number of measurements: {len(self.__measurements)}")

        if len(self.__measurements) == 0:
            return

        # If there are any measurements in the file, print stats for the
        # first one
        m = self.__measurements[0]
        print("  ⟡ 2θ area: {0:.5f}°–{1:.5f}° (Δ {2:.5f}°)"
            .format(
                m.two_theta_start,
                m.two_theta_end,
                m.two_theta_step
            ))

        Fmt.minor(f"  Writing ASC file '{self.__output_path}'.")
        result = self.__write_asc_file()


class Debug:
    """Utilities for printing debug logs."""

    # Whether printing debug messages is enabled
    is_enabled = False

    # Debug marker that is prefixed to debug logs
    debug_marker = "DEBUG:"

    def log(*args):
        """Prints a debug log if debugging is enabled (otherwise the
        message is discarded).

        Arguments are passed to the `print` function and are prefixed by
        a debug marker.
        """
        if Debug.is_enabled:
            Fmt.debug(Debug.debug_marker, *args)


class Fmt:
    """Utilities for colourized and otherwise formatted text output."""

    # Whether text formatting is enabled
    is_enabled = False

    # Format code constants
    # Reset
    RESET = "\033[0;0m"
    # Regular colours
    GREEN  = "\033[0;32m"
    PURPLE = "\033[0;35m"
    # Bold colours
    BOLD_RED    = "\033[1;31m"  
    BOLD_BLUE   = "\033[1;34m"
    BOLD_CYAN   = "\033[1;36m"
    BOLD_YELLOW = "\033[1;33m"
    BOLD_GREEN = "\033[1;32m"
    # Formatting
    BOLD    = "\033[;1m"
    REVERSE = "\033[;7m"
    # Special

    def print(format_code: str, reset: bool, *args):
        """If formatting is enabled, formats `*args` by prepending it
        `format_code` and applying reset sequence afterwards, if `reset`
        is `True`. Contents are printed using `print` function.

        If formatting is disabled, `*args` are simply forwarded to `print`.
        """
        if Fmt.is_enabled == True:
            print(format_code, end='')
            print(*args, end='')

            if reset == True:
                print(Fmt.RESET)
            else:
                print()
        else:
            print(*args)

    #
    # Formatting shorthands
    #

    def greeting(*args):
        Fmt.print(Fmt.BOLD_YELLOW, True, *args)

    def error(*args):
        Fmt.print(Fmt.BOLD_RED, True, *args)

    def debug(*args):
        Fmt.print(Fmt.PURPLE, True, *args)

    def major(*args):
        Fmt.print(Fmt.BOLD_GREEN, True, *args)

    def minor(*args):
        Fmt.print(Fmt.BOLD_BLUE, True, *args)


#------------------------------------------------------------------------------
#     FUNCTIONS
#------------------------------------------------------------------------------

def print_greeting():
    Fmt.greeting(f"{Application.NAME} version "
        f"{Application.VERSION} "
        f"({Application.DATE})\n")


def print_help():
    print("Usage:")
    print("python xrdml2asc.py [-w] xrdml_file_1 xrdml_file_2 ...")
    print("python xrdml2asc.py [-w] -e extension")
    print("")
    print("-w: overwrite output file if it already exists")
    print("-e: process all files with given extension")
    print("-h: print this message")
    print("-d: enable debug logs")
    print("-c: enable coloured output")
    print("")


def validate_extension(extension: str) -> str:
    """ Returns a valid extension (lower case preceeded by '.') or an empty
    string if the extension is invalid (contains characters other than
    letters, numbers or '.').
    """
    if len(extension) == 0:
        return ""
    
    for c in extension:
        if not c.isalpha() and c != '.':
            return ""
    
    new_extension = extension.lower()
    if new_extension[0] != '.':
        new_extension = '.' + new_extension
    return new_extension


def filename_matches_extension(file_name: str, extension: str) -> bool:
    """ Checks whether file name matches given extension regardless of
    the case of letters.
    """
    if file_name.lower().endswith(extension):
        return True
    else:
        return False


def generate_new_filename(
    old_filename: str,
    new_extension: str,
    old_extension = ""
) -> str:
    """ Generates new file name by replacing its old extension with new
    extension. If old extension is not provided, everything after the last '.'
    is treated as extension.

    File name with new extension is returned. In case of errors, an empty
    string is returned.

    Extensions should include '.'.
    """
    # Use provided old extension
    if len(old_extension) > 0:
        i = old_filename.rfind(old_extension)
        if (i == len(old_filename) - len(old_extension)):
            new_filename = old_filename[:i] + new_extension
            return new_filename
        else:
            return ""
    # Determine old extension
    else:
        i = old_filename.rfind(".")
        # First character '.' should not get replaced
        if i > 1:
            new_filename = old_filename[:i] + new_extension
            return new_filename
        else:
            return ""


#------------------------------------------------------------------------------
#     MAIN
#------------------------------------------------------------------------------

if __name__ == "__main__":

    #
    # Global variables
    #

    # Whether files are to be selected by their extension rather
    # than their name
    extension_mode = False
    # Extension used to determine input file names
    extension = None
    # Whether to overwrite files when exporting
    overwrite = False
    # List of files to process
    file_list = []

    #
    # Argument parsing
    #

    args = sys.argv[1:]
    options = "hwdce:"
    try:
        opt_val_pairs, trailing_args = getopt.getopt(args, options)
    except getopt.GetoptError as e:
        Fmt.error("Error when parsing arugments:", str(e))
        sys.exit(1)

    # Print help and exit if no args are present
    if len(opt_val_pairs) == 0 and len(trailing_args) == 0:
        print_help()
        sys.exit()

    # Process colour output flag before displaying the greeting
    if ("-c", "") in opt_val_pairs:
        Fmt.is_enabled = True

    # Process arguments ...
    # ... and print the greeting of course
    print_greeting()
    
    for option, value in opt_val_pairs:
        # -h: help
        if option == "-h":
            print_help()
            sys.exit()
        # -w: overwrite
        elif option == "-w":
            overwrite = True
            Debug.log("Overwrite enabled.")
        # -e: extension mode
        elif option == "-e":
            extension_mode = True
            extension = validate_extension(value)
            if extension == "":
                Fmt.error(f"Invalid extension '{value}'.")
                sys.exit(1)
        # -d: debug
        elif option == "-d":
            Debug.is_enabled = True
            Debug.log("Debug logs enabled.")
        # -c: colour output is processed beforehand
        elif option == "-c":
            pass
        # Invalid argument
        else:
            Fmt.error(f"Invalid argument '{option}'.")
            sys.exit(1)
    
    # Ignore trailing args in extension mode
    if extension_mode == True and len(trailing_args) > 0:
        Fmt.error(f"{len(trailing_args)} redundant trailing arguments will be "
            "ignored in extension mode.")

    #
    # Generate file list
    #
    
    # Normal mode: make filelist from trailing args
    if extension_mode == False:
        for arg in trailing_args:
            if os.path.isfile(arg):
                file_list.append(arg)
            else:
                Fmt.error(f"'{arg}' is not a file and will be ignored.")
    # Extension mode: make filelist by filtering current directory contents
    else:
        dir_list = os.listdir()
        for entry in dir_list:
            if (os.path.isfile(entry) and
                    filename_matches_extension(entry, extension)):
                file_list.append(entry)

    #
    # Process files
    #

    print(f"Number of files to process: {len(file_list)}\n")

    for input_file in file_list:
        # Generate new filename
        if extension == None:
            output_file = generate_new_filename(input_file, ".asc")
        else:
            output_file = generate_new_filename(input_file, ".asc", extension)

        # Check filename validity
        if output_file == "":
            Fmt.error("Could not generate output file name for file "
                f"'{input_file}'. The file will be skipped.")
            continue

        # Check possible overwrite
        if overwrite == False:
            if os.path.exists(output_file):
                Fmt.error(f"File '{output_file}' already exists and will not "
                    "be overwritten.")
                continue

        try:
            Fmt.major(f"⬥ Processing file '{input_file}'")
            xrdml2asc = Xrdml2Asc(input_file, output_file)
            xrdml2asc.convert_file()
        except IOError as e:
            Fmt.error("Error while working with files:", str(e))
        except XrdmlException as e:
            Fmt.error("Error in XRDML parse process:", str(e))
