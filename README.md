# xrdml2asc

A Python script for conversion of powder X-ray diffraction data files from XRDML format to plain data points (ASC).


## Requirements

[Python 3](https://www.python.org/) interpreter.


## Usage

Run the script with the `-h` flag to display instructions.

```sh
python xrdml2asc.py -h
```

To process all XRDML files in the current directory use the `-e` option.

```sh
python xrdml2asc.py -e ".xrdml"
```
